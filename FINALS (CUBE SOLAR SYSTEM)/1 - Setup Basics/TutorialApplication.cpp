/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreManualObject.h>
#include "Planet.h"

TutorialApplication::TutorialApplication(void)
{
}
TutorialApplication::~TutorialApplication(void)
{
}
//MOVEMENT
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	sun->movement(sun->getNode(), Degree(0 * evt.timeSinceLastFrame), Degree(30 * evt.timeSinceLastFrame));
	mercury->movement(mercury->getNode(), Degree(28.2f * evt.timeSinceLastFrame), Degree(45.0f * evt.timeSinceLastFrame));
	venus->movement(venus->getNode(), Degree(9.9f * evt.timeSinceLastFrame), Degree(45.0f * evt.timeSinceLastFrame));
	earth->movement(earth->getNode(), Degree(6.08f * evt.timeSinceLastFrame), Degree(45.0f * evt.timeSinceLastFrame));
	mars->movement(mars->getNode(), Degree(3.23f * evt.timeSinceLastFrame), Degree(45.0f * evt.timeSinceLastFrame));

	return true;
}
//SCENE
void TutorialApplication::createScene(void)
{
	sun = Planet::createAstrobody(*mSceneMgr, 20, ColourValue(1, 1, 0), 0, false, "Sun");
	mercury = Planet::createAstrobody(*mSceneMgr, 3, ColourValue(0.82f, 0.7f, 0.54f), 50, true, "Mercury");
	earth = Planet::createAstrobody(*mSceneMgr, 5, ColourValue::Blue, 100, true, "Earth");
	venus = Planet::createAstrobody(*mSceneMgr, 10, ColourValue(0.93, 0.9f, 0.67f), 150, true, "Venus");
	mars = Planet::createAstrobody(*mSceneMgr, 8, ColourValue(0.71f, 0.25f, 0.05f), 250, true, "Mars");
}

//---------------------------------------------------------------------------
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
