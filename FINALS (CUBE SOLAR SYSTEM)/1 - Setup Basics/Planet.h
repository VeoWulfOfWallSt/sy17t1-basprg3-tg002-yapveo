#pragma once
#include "BaseApplication.h"
using namespace Ogre;

class Planet
{
public:
	Planet();
	
	Planet(SceneNode *node);
	void setNode(SceneNode*planet);
	SceneNode * getNode();
	//Create Planet
	static Planet* createAstrobody(SceneManager & sceneManager, float size, ColourValue colour, float translate, bool light, String name);
	void movement(SceneNode *planet, Degree revolve, Degree rotate);
	~Planet();
private :
	SceneNode * mNode;
	SceneNode * mMove;
};

