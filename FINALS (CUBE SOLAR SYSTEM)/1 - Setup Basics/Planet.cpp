#include "Planet.h"


Planet::Planet()
{
}

Planet::Planet(SceneNode * planet)
{
	mNode = planet;
}

void Planet::setNode(SceneNode * planet)
{
	mNode = planet;
}

SceneNode * Planet::getNode()
{
	return mNode;
}



Planet * Planet::createAstrobody(SceneManager & sceneManager, float size, ColourValue colour, float translate, bool light, String name)
{
	SceneNode*node = sceneManager.getRootSceneNode()->createChildSceneNode();
	ManualObject* manual = sceneManager.createManualObject();
	Planet* planet = new Planet;

	// CHECKER IF SUNLIGHT HITS THE PLANET
	if (light)
	{
		Light* pointLight = sceneManager.createLight();
		pointLight->setType(Light::LT_POINT);
		pointLight->setPosition(translate, translate, translate);
		pointLight->setDiffuseColour(1, 1, 0.);
		pointLight->setSpecularColour(ColourValue::White);
		pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
		pointLight->setCastShadows(false);
		sceneManager.setAmbientLight(ColourValue(0.1, 0.1, 0.1));


		MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");
		myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);
		myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(colour);

		manual->begin(name, RenderOperation::OT_TRIANGLE_LIST);
	}
	else
	{
		manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	}

	//FRONT
	manual->position(-size / 2, -size / 2, size / 2);
	manual->normal(Vector3(0, 0, 1));
	manual->colour(colour);
	manual->position(size / 2, -size / 2, size / 2);
	manual->normal(Vector3(0, 0, 1));
	manual->position(size / 2, size / 2, size / 2);
	manual->normal(Vector3(0, 0, 1));
	manual->position(-size / 2, size / 2, size / 2);
	manual->normal(Vector3(0, 0, 1));

	//BACK
	manual->position(size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-size / 2, size / 2, -size / 2);
	manual->normal(Vector3(0, 0, -1));
	manual->position(size / 2, size / 2, -size / 2);
	manual->normal(Vector3(0, 0, -1));

	//TOP
	manual->position(-size / 2, size / 2, size / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size / 2, size / 2, size / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size / 2, size / 2, -size / 2);
	manual->normal(Vector3(0, 1, 0));
	manual->position(-size / 2, size / 2, -size / 2);
	manual->normal(Vector3(0, 1, 0));

	//BOTTOM
	manual->position(-size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size / 2, -size / 2, size / 2);
	manual->normal(Vector3(0, -1, 0));
	manual->position(-size / 2, -size / 2, size / 2);
	manual->normal(Vector3(0, -1, 0));

	//LEFT
	manual->position(-size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, -size / 2, size / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, size / 2, size / 2);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, size / 2, -size / 2);
	manual->normal(Vector3(-1, 0, 0));

	//RIGHT
	manual->position(size / 2, -size / 2, size / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, -size / 2, -size / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, size / 2, -size / 2);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, size / 2, size / 2);
	manual->normal(Vector3(1, 0, 0));



	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(2);
	manual->index(3);
	manual->index(0);


	manual->index(4);
	manual->index(5);
	manual->index(6);
	manual->index(6);
	manual->index(7);
	manual->index(4);


	manual->index(8);
	manual->index(9);
	manual->index(10);
	manual->index(10);
	manual->index(11);
	manual->index(8);


	manual->index(12);
	manual->index(13);
	manual->index(14);
	manual->index(14);
	manual->index(15);
	manual->index(12);


	manual->index(16);
	manual->index(17);
	manual->index(18);
	manual->index(18);
	manual->index(19);
	manual->index(16);


	manual->index(20);
	manual->index(21);
	manual->index(22);
	manual->index(22);
	manual->index(23);
	manual->index(20);

	manual->end();



	node->attachObject(manual);
	planet->setNode(node);
	node->translate(translate, 0, 0);





	return planet;
}

void Planet::movement(SceneNode *planet ,Degree revol, Degree rots)
{
	mMove = planet;

	float oldXrev = mMove->getPosition().x;
	float oldZrev = mMove->getPosition().z;

	//Rotate
	mMove->yaw(Radian(rots));
	//Revolve
	float newXrev = (oldXrev * Math::Cos(Radian(revol))) + (oldZrev * Math::Sin(Radian(revol)));
	float newZrev = (oldXrev * -Math::Sin(Radian(revol))) + (oldZrev * Math::Cos(Radian(revol)));

	/*Vector3 pos = Vector3(newXrev, mMove->getPosition().y, newZrev);
	Vector3 newPos = pos + base->getPosition();*/
	mMove->setPosition(newXrev, 0, newZrev);
	//mMove->setPosition(newPos);
}

Planet::~Planet()
{
}