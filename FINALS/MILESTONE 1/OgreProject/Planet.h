#pragma once
#include "BaseApplication.h"
#include "TutorialApplication.h"
#include <OgreSceneNode.h>
#include <OgreManualObject.h>
#include <string>
#include <iostream>
using namespace Ogre;
class Planet:public BaseApplication
{
public:
	Planet();
	Planet(char name);
	void createCube(float size);

	~Planet();
};

