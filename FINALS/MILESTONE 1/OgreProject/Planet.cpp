#include "Planet.h"



Planet::Planet()
{
}

Planet::Planet(char name)
{
	char mName = name;
	
}

void Planet::createCube(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject("manual");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);


	manual->position(-(size / 2), -(size / 2), size / 2);//0
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), -(size / 2), size / 2);//1
	manual->normal(Vector3(1, 0, 0));
	manual->position((size / 2), (size / 2), size / 2);//2
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(size / 2), (size / 2), size / 2);//3
	manual->normal(Vector3(0, 1, 0));

	manual->position(-(size / 2), (size / 2), -(size / 2));//4
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), (size / 2), -(size / 2));//5
	manual->normal(Vector3(1, 0, 0));
	manual->position((size / 2), -(size / 2), -(size / 2));//6
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(size / 2), -(size / 2), -(size / 2));//7
	manual->normal(Vector3(0, 1, 0));

	manual->position((size / 2), (size / 2), size / 2);//8
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), -(size / 2), size / 2);//9
	manual->normal(Vector3(1, 0, 0));
	manual->position(-(size / 2), -(size / 2), size / 2);//10
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(size / 2), (size / 2), size / 2);//11
	manual->normal(Vector3(0, 1, 0));

	manual->position(-(size / 2), (size / 2), -(size / 2));//12
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), (size / 2), -(size / 2));//13
	manual->normal(Vector3(1, 0, 0));
	manual->position((size / 2), -(size / 2), -(size / 2));//14
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(10 / 2), -(10 / 2), -(10 / 2));//15
	manual->normal(Vector3(0, 1, 0));

	manual->position((size / 2), (size / 2), size / 2);//16
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), -(size / 2), size / 2);//17
	manual->normal(Vector3(1, 0, 0));
	manual->position(-(size / 2), -(size / 2), size / 2);//18
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(10 / 2), (10 / 2), 10 / 2);//19
	manual->normal(Vector3(0, 1, 0));

	manual->position(-(size / 2), (size / 2), -(size / 2));//20
	manual->normal(Vector3(0, 0, 1));
	manual->position((size / 2), (size / 2), -(size / 2));//21
	manual->normal(Vector3(1, 0, 0));
	manual->position((size / 2), -(size / 2), -(size / 2));//22
	manual->normal(Vector3(0, 1, 0));
	manual->position(-(size / 2), -(size / 2), -(size / 2));//23
	manual->normal(Vector3(0, 1, 0));

	//Index

	manual->index(0);
	manual->index(1);
	manual->index(2);
	manual->index(3);
	manual->index(0);
	manual->index(2);

	manual->index(11);
	manual->index(12);
	manual->index(15);
	manual->index(15);
	manual->index(10);
	manual->index(11);

	manual->index(4);
	manual->index(5);
	manual->index(23);
	manual->index(23);
	manual->index(5);
	manual->index(6);

	manual->index(8);
	manual->index(9);
	manual->index(13);
	manual->index(13);
	manual->index(9);
	manual->index(14);

	manual->index(16);
	manual->index(21);
	manual->index(19);
	manual->index(19);
	manual->index(21);
	manual->index(20);

	manual->index(17);
	manual->index(18);
	manual->index(22);
	manual->index(22);
	manual->index(18);
	manual->index(23);





	manual->end();
	

	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
}






Planet::~Planet()
{
}
