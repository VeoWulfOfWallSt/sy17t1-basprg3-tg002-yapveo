/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include <OgreSceneNode.h>
#include <OgreManualObject.h>

using namespace Ogre;

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{

}
void TutorialApplication::createCube(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject();
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	manual->position(size, size, 0.0); // NE
	manual->colour(0, 0, 255);
	manual->position(size, -size, 0.0); // SE
	manual->colour(ColourValue::Blue);
	manual->position(-size, -size, 0); // SW
	manual->colour(ColourValue::Blue);
	manual->position(-size, size, 0); // NW
	manual->colour(ColourValue::Blue);

	manual->position(size, size, -size*2); // NE
	manual->colour(ColourValue::Green);
	manual->position(size, -size, -size * 2); // SE
	manual->colour(ColourValue::Green);
	manual->position(-size, -size, -size * 2); // SW
	manual->colour(ColourValue::Green);
	manual->position(-size, size, -size * 2); // NW
	manual->colour(ColourValue::Green);


									   // Index buffer
	manual->index(0);
	manual->index(3);
	manual->index(2);
	manual->index(0);
	manual->index(2);
	manual->index(1);

	manual->index(0);
	manual->index(5);
	manual->index(4);
	manual->index(0);
	manual->index(1);
	manual->index(5);

	manual->index(3);
	manual->index(6);
	manual->index(2);
	manual->index(3);
	manual->index(7);
	manual->index(6);

	manual->index(6);
	manual->index(4);
	manual->index(5);
	manual->index(7);
	manual->index(4);
	manual->index(6);

	manual->index(0);
	manual->index(7);
	manual->index(3);
	manual->index(0);
	manual->index(4);
	manual->index(7);

	manual->index(1);
	manual->index(2);
	manual->index(6);
	manual->index(1);
	manual->index(6);
	manual->index(5);





	manual->end();
	SceneNode* scene = mSceneMgr->getRootSceneNode();
	SceneNode* triangle = scene->createChildSceneNode();
	triangle->attachObject(manual);

}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	createCube(10);
	
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent &arg)
{
	BaseApplication::keyPressed(arg);
	SceneNode*mObject;
	// Lecture: Introduce keyPressed first
	if (arg.key == OIS::KC_T) {
		mObject->translate(0, 0, 1);
	}
	else if (arg.key == OIS::KC_G) {
		mObject->translate(0, 0, -1);
	}

	return true;
}

/// This function is called every 'frame' (see slides). This means you can constantly check if a key is still being pressed
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	SceneNode*mObject;
	// Use the keyboard object (from BaseApplication) to check if a key is being pressed
	if (mKeyboard->isKeyDown(OIS::KC_J)) {
		// You have to multiply the magnitude of movement by the delta tiem (time since last frame) so that it will sync with the machine's frame rate
		mObject->translate(10 * evt.timeSinceLastFrame, 0, 0);
	}
	if (mKeyboard->isKeyDown(OIS::KC_L)) {
		mObject->translate(10 * evt.timeSinceLastFrame, 0, 0);
	}
	return true;
}



#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
